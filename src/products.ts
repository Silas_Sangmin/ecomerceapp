//create an object interface of product
export interface Product{
    id: number;
    name: string;
    price: number;
    description: string;
    imgUrl: string;
    qty:number;
}

//create an array of product objects
export const products = [
    {
      id: 1,
      name: 'Tecno Spark 5 Pro',
      price: 2500,
      description: ' 64GB HDD - 3GB RAM Smartphone - Ice Jadeite',
      imgUrl:'https://qenwa-s3-cdn.s3.eu-central-1.amazonaws.com/wp-content/uploads/2020/12/16085228/tecno-spark-5-pro-Ice-Jadeite.jpg',
      qty: 1,
    },
    {
      id: 2,
      name: 'iPhone 12',
      price: 9547,
      description: 'A great phone with one of the best cameras. 512GB HDD - 6GB RAM - Gold,Brand: Apple',
      imgUrl: 'https://www.telefonika.com/wp-content/uploads/2020/11/Apple-iPhone-12-Pro-Blue.jpg',
      qty: 1,
    },
    {
      id: 3,
      name: 'Samsung S21 Ultra ',
      price: 7999,
      description: '256GB HDD - 12GB RAM Smartphone - Black. Brand: Samsung ',
      imgUrl: 'https://www.mytrendyphone.eu/images/Samsung-Galaxy-S21-Ultra-5G-128GB-Phantom-Black-8806090887505-15012021-01-p.jpg',
      qty: 1,
    }
  ];