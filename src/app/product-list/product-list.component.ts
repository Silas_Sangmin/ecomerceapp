import { CartService } from './../cart.service';
import { Product, products } from './../../products';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products = products;
  product: Product | undefined;

  addToCart(product: Product) {
    this.cartService.addToCart(product);
    alert('The product has been added to your cart.')
  }
  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }

}
