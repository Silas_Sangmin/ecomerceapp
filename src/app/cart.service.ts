import { Product } from './../products';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class CartService {

  //create an array of items to store products in cart
  items: Product[] = [];


  constructor(private http: HttpClient) { }


  //add products to cart
  addToCart(product: Product) {
    if (this.items.indexOf(product) == -1) {
      this.items.push(product);
    }

  }

  //get items in cart
  getItems() {
    return this.items;
  }

  //get shipping prices from json data file
  getShippingPrices() {
    return this.http.get<{ type: string, price: number }[]>
      ('/assets/shipping.json');
  }

}
