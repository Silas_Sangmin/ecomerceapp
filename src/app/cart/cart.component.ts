import { Product } from './../../products';
import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  constructor(
    private cartService: CartService, private formBuilder: FormBuilder) { }

  items = this.cartService.getItems();
  product!: Product;
  
  checkoutForm = this.formBuilder.group({
    name: '',
    address: ''
  });

  onSubmit(): void {
    //checkout processing here
    this.items = this.clearCart();
    alert('Your order has been submitted');
    this.checkoutForm.reset();
  }

  addQty(product:Product):number{
    return product.qty += 1;
  }

  decreaseQty(product:Product):number{
    if (product.qty > 1) {
      return product.qty -= 1;
    }
    return 1;
  }

 
  removeProduct(product:Product): void{
    this.items.splice(this.items.indexOf(product), 1);
  }

   //empty cart
   clearCart() {
    this.items = [];
    return this.items;
  }


  ngOnInit(): void {
  }

}
